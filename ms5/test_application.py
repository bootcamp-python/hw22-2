import pytest, json, mock, requests
import application.app as app

class ResponseMock(requests.Response):
    '''
    Заглушка для тестирования http запросов
    '''
    status_code = 200
    _content_consumed = True
    raw = b'123'
    def __init__(self):
        pass
    @property
    def text(self) -> str:
        return '<html><head></head><body><audio><source src="//upload/hymn.mp3"/></audio></body></html>'
    @property
    def content(self) -> bytes:
        return b'123'

def test_run():
    # Гимн не найден
    with mock.patch('smtplib.SMTP_SSL') as smtp_ssl_mock:
        with mock.patch('requests.get', return_value = ResponseMock()) as requests_get_mock:
            body = str.encode(json.dumps({'country': 'hahaha', 'url': '', 'email': 'user@user.ru'}))
            app.run(None, None, None, body)
            # по идее надо проверять письмо (есть ли вложение и т.п.), но пока так
            requests_get_mock.assert_not_called()
            context = smtp_ssl_mock.return_value.__enter__.return_value
            context.login.assert_called_with(app.conf.SMTP_USER, app.conf.SMTP_PASS)
            context.send_message.assert_called()
            assert context.login.call_count == 1, 'Авторизация smtp вызвана некорректное количество раз'
            assert context.send_message.call_count == 1, 'Отправка отчета вызвана некорректное количество раз'
    # Гимн найден
    with mock.patch('smtplib.SMTP_SSL') as smtp_ssl_mock:
        with mock.patch('requests.get', return_value = ResponseMock()) as requests_get_mock:
            body = str.encode(json.dumps({'country': 'russia', 'url': 'http://russia', 'email': 'user@user.ru'}))
            app.run(None, None, None, body)
            # по идее надо проверять письмо (есть ли вложение и т.п.), но пока так
            requests_get_mock.assert_called()
            assert requests_get_mock.call_count == 2
            context = smtp_ssl_mock.return_value.__enter__.return_value
            context.login.assert_called_with(app.conf.SMTP_USER, app.conf.SMTP_PASS)
            context.send_message.assert_called()
            assert context.login.call_count == 1, 'Авторизация smtp вызвана некорректное количество раз'
            assert context.send_message.call_count == 1, 'Отправка отчета вызвана некорректное количество раз'

if __name__ == "__main__":
    pytest.main()
