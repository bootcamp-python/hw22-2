import os

class Config:
    DEBUG   = False
    TESTING = False

    SENTRY_DSN = os.getenv('SENTRY_DSN')

    RMQ_HOST = os.getenv('RMQ_HOST')
    RMQ_PORT = os.getenv('RMQ_PORT')
    RMQ_USER = os.getenv('RMQ_USER')
    RMQ_PASS = os.getenv('RMQ_PASS')
    RMQ_URL  = f'amqp://{RMQ_USER}:{RMQ_PASS}@{RMQ_HOST}:{RMQ_PORT}'

    SMTP_HOST = os.getenv('SMTP_HOST')
    SMTP_PORT = os.getenv('SMTP_PORT')
    SMTP_USER = os.getenv('SMTP_USER')
    SMTP_PASS = os.getenv('SMTP_PASS')


class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True
