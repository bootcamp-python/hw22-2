import os, pika, sentry_sdk, json, requests, os, smtplib, mimetypes
from time import sleep
from bs4 import BeautifulSoup
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.audio import MIMEAudio
from email import encoders
import config

flask_env = os.getenv('FLASK_ENV', None)
if flask_env == 'development':
    conf = config.DevelopmentConfig()
elif flask_env == 'testing':
    conf = config.TestingConfig()
elif flask_env == 'production':
    conf = config.ProductionConfig()
else:
    raise Exception('Environment not loaded')

sentry_sdk.init(conf.SENTRY_DSN, traces_sample_rate=1.0, server_name='ms5')
sentry_sdk.capture_message(f'ms5: init')

def run(ch, method, properties, body):
    '''
    Обработка сообщения из очереди
    '''
    data = json.loads(body.decode())
    country, url, email = data['country'], data['url'], data['email']
    sentry_sdk.capture_message(f'ms5: скачивание гимна и отправка на почту ({country}, {url}, {email})')
    audio_data = None
    if url:
        # Ищем ссылку на аудиофайл
        audio = BeautifulSoup(requests.get(url).text, 'html.parser').find('audio')
        if audio:
            audio_url = 'http:' + audio.find('source').get('src')
            # Загружаем аудио
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:95.0) Gecko/20100101 Firefox/95.0',
                'Accept': '*/*;q=0.8'
            }
            with requests.get(audio_url, headers=headers) as req:
            # with requests.get(audio_url, stream=True, headers=headers) as req:
                audio_data = req.content
                # audio_data = None
                # for chunk in req.iter_content(chunk_size=1024): 
                #     if chunk: # filter out keep-alive new chunks
                #         audio_data = (audio_data + chunk) if audio_data else chunk
    # Отправка письма на почту
    if audio_data:
        body_html =  f'Гимн для страны "{country}" во вложении<br><br>'
        body_html += f'Страна: <a href="{url}">{url}</a><br>'
        body_html += f'Аудиофайл: <a href="{audio_url}">{audio_url}</a>'
    else:
        body_html = f'Гимн для страны "{country}" не найден'
    msg = MIMEMultipart()
    msg['Subject'] = f'Гимн страны "{country}"'
    msg['From'] = conf.SMTP_USER
    msg['To'] = email
    msg.attach(MIMEText(body_html, 'html'))
    if audio_data:
        audio_type = mimetypes.guess_type(audio_url)
        part = MIMEAudio(audio_data, audio_type[0].split('/')[1])
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment', filename=os.path.basename(audio_url))
        msg.attach(part)
    with smtplib.SMTP_SSL(conf.SMTP_HOST, conf.SMTP_PORT) as smtp:
        smtp.login(conf.SMTP_USER, conf.SMTP_PASS)
        smtp.send_message(msg)

if conf.TESTING:
    rmq_channel = None
else:
    sentry_sdk.capture_message(f'ms5: RMQ_URL={conf.RMQ_URL}')
    sleep(20) # задержка чтобы другие сервисы успели запуститься
    rmq_parameters = pika.URLParameters(conf.RMQ_URL)
    rmq_connection = pika.BlockingConnection(rmq_parameters)
    rmq_channel = rmq_connection.channel()
    rmq_channel.queue_declare(queue='urls')
    rmq_channel.basic_consume(queue='urls', consumer_tag='ms5', on_message_callback=run, auto_ack=True)
