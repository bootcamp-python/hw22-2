import pytest, mock, json
from application.app import app, db
from application.database import HymnRequest

@pytest.fixture
def client():
    yield app.test_client()
    truncate_DB()

def truncate_DB():
    with app.app_context():
        HymnRequest.query.delete()
        db.session.commit()

def test_index(client):
    with app.app_context():
        # Не передали параметры
        response = client.get('/')
        assert response.status_code == 200, 'Код возврата не 200'
        assert response.json['message'] == 'invalid parameters', 'Некорректная реакция на отсутствие параметров'
        # Нет страны в кэше
        with mock.patch('application.routes.rds_cache_get', return_value = None) as rds_cache_get_mock:
            with mock.patch('application.routes.rmq_channel_basic_publish') as rmq_channel_basic_publish_mock:
                response = client.get('/', query_string={'country': 'russia', 'email': 'admin@admin.ru'})
                rds_cache_get_mock.assert_called_once_with('russia')
                body = json.dumps({'country': 'russia', 'email': 'admin@admin.ru'})
                rmq_channel_basic_publish_mock.assert_called_once_with('countries', body)
                assert HymnRequest.query.count() == 1, 'Запрос гимна не появился в БД'
        # Есть страна в кэше
        with mock.patch('application.routes.rds_cache_get', return_value = 'http://russia') as rds_cache_get_mock:
            with mock.patch('application.routes.rmq_channel_basic_publish') as rmq_channel_basic_publish_mock:
                response = client.get('/', query_string={'country': 'russia', 'email': 'admin@admin.ru'})
                rds_cache_get_mock.assert_called_once_with('russia')
                body = json.dumps({'country': 'russia', 'url': 'http://russia', 'email': 'admin@admin.ru'})
                rmq_channel_basic_publish_mock.assert_called_once_with('urls', body)
                assert HymnRequest.query.count() == 2, 'Запрос гимна не появился в БД'

if __name__ == "__main__":
    pytest.main()
