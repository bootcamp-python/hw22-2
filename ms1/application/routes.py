import sentry_sdk, json
from flask import Blueprint, jsonify, request
from application.app import db
from application.database import HymnRequest

view = Blueprint('view', __name__)

@view.route('/', methods=['GET'])
def index():
    '''
    Запрос гимна
    '''
    try:
        # Проверка параметров
        country = request.args.get('country')
        email = request.args.get('email')
        sentry_sdk.capture_message(f'ms1: запрос гимна ({country}, {email})')
        if not country or not email:
            return jsonify({'message': 'invalid parameters'})
        country_l = country.lower()
        # Сохранение в БД
        db.session.add(HymnRequest(country=country, email=email))
        db.session.commit()
        # Поиск в кэше
        country_url = rds_cache_get(country_l)
        if country_url:
            # Найдено в кэше
            body = json.dumps({'country': country, 'url': country_url, 'email': email})
            rmq_channel_basic_publish('urls', body)
        else:
            # Не найдено в кэше
            rmq_channel_basic_publish('countries', json.dumps({'country': country, 'email': email}))
        return jsonify({'message': 'hymn request received'})
    except Exception as err:
        return jsonify({'message': f'error: {err}'})

def rds_cache_get(country: str) -> str:
    '''
    Поиск в кэше Redis
    '''
    from application.app import rds_cache
    try:
        res = rds_cache.get(country)
    except Exception as err:
        sentry_sdk.capture_message(f"ms1: ошибка при обращении к Reddis: {err}")
        res = None

    if res:
        return res.decode()
    else:
        return None
    
def rmq_channel_basic_publish(routing_key: str, body: str) -> None:
    '''
    Помещение сообщения в очередь RabbitMQ
    '''
    from application.app import rmq_channel
    rmq_channel.basic_publish(exchange='', routing_key=routing_key, body=body)
