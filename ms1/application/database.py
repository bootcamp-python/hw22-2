import datetime
from application.app import db

class HymnRequest(db.Model):
    '''
    Запрос гимна
    '''
    __tablename__ = 'hymn_requests'
    id      = db.Column(db.Integer, primary_key=True, unique=True)
    country = db.Column(db.String(50), nullable=False)
    email   = db.Column(db.String(50), nullable=False)
    dt      = db.Column(db.DateTime, nullable=False, default=datetime.datetime.today())

    def __init__(self, country, email) -> None:
        self.country = country
        self.email = email

    def __str__(self) -> str:
        return f'{self.id}'
