import redis, pika, sentry_sdk
from time import sleep
from flask_sqlalchemy import SQLAlchemy
from application import create_app

db = SQLAlchemy()
app = create_app(db)

sentry_sdk.init(app.config['SENTRY_DSN'], traces_sample_rate=1.0, server_name='ms1')
sentry_sdk.capture_message(f"ms1: init")

if app.config['TESTING']:
    rds_cache = None
    rmq_channel = None
else:
    sentry_sdk.capture_message(f"ms1: RDS_HOST={app.config['RDS_HOST']}, RDS_PORT={app.config['RDS_PORT']}, RMQ_URL={app.config['RMQ_URL']}")
    sleep(20) # задержка чтобы другие сервисы успели запуститься
    rds_cache = redis.Redis(host=app.config['RDS_HOST'], port=app.config['RDS_PORT'])
    rmq_parameters = pika.URLParameters(app.config['RMQ_URL'])
    rmq_connection = pika.BlockingConnection(rmq_parameters)
    rmq_channel = rmq_connection.channel()
    rmq_channel.queue_declare(queue='urls')
    rmq_channel.queue_declare(queue='countries')

with app.app_context():
    db.create_all()
