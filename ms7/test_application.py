import pytest, mock
import application.app as app
import application.database as db

def test_get_html_table():
    # проверим что в принципе есть какая-то таблица
    html = app.get_html_table()
    assert html.strip()[:6] == '<table', 'Некорректное начало html таблицы'
    assert html.strip()[-8:] == '</table>', 'Некорректное окончание html таблицы'

def test_get_pdf():
    with mock.patch('application.app.get_pdf', return_value='<b>test</b>'):
        assert app.get_pdf() != None, 'Не сформирован pdf'

def test_send_report():
    app.db_session.add(db.Admin(email='admin1@admin.ru'))
    app.db_session.add(db.Admin(email='admin2@admin.ru'))
    app.db_session.add(db.HymnRequest(country='russia', email='user@user.ru'))
    app.db_session.add(db.HymnRequest(country='latvia', email='user@user.ru'))
    app.db_session.add(db.HymnRequest(country='12345', email='user@user.ru'))
    app.db_session.commit()
    with mock.patch('smtplib.SMTP_SSL') as smtp_ssl_mock:
        app.send_report()
        context = smtp_ssl_mock.return_value.__enter__.return_value
        context.login.assert_called_with(app.conf.SMTP_USER, app.conf.SMTP_PASS)
        context.send_message.assert_called()
        assert context.login.call_count == 2, 'Авторизация smtp вызвана некорректное количество раз'
        assert context.send_message.call_count == 2, 'Отправка отчета вызвана некорректное количество раз'

if __name__ == "__main__":
    pytest.main()
