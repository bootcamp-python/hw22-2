import os, sentry_sdk
import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base
from time import sleep
from fpdf import FPDF, HTMLMixin

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email import encoders

import config

flask_env = os.getenv('FLASK_ENV', None)
if flask_env == 'development':
    conf = config.DevelopmentConfig()
elif flask_env == 'testing':
    conf = config.TestingConfig()
elif flask_env == 'production':
    conf = config.ProductionConfig()
else:
    raise Exception('Environment not loaded')

sentry_sdk.init(conf.SENTRY_DSN, traces_sample_rate=1.0, server_name='ms7')
sentry_sdk.capture_message(f'ms7: init 1')

if not conf.TESTING:
    sleep(10) # задержка для ожидания запуска других сервисов

engine = sa.create_engine(conf.SQLALCHEMY_DATABASE_URI)
db_session = orm.scoped_session(orm.sessionmaker(bind=engine))
db_model = declarative_base()
db_model.query = db_session.query_property()
db_model.metadata.bind = engine

# Импорт сущностей только после инициализации db_model
from application.database import Admin, HymnRequest

db_model.metadata.create_all(bind=engine)

# Пока пусть так )
if not conf.TESTING:
    db_session.add(Admin(email='mail@igorguryanov.ru'))
    db_session.add(Admin(email='admin@igorguryanov.ru'))
    db_session.commit()

sentry_sdk.capture_message(f'ms7: init 2')

def job():
    '''
    Периодически запускаемая функция
    '''
    admins_count = Admin.query.count()
    requests_count = HymnRequest.query.count()
    sentry_sdk.capture_message(f'ms7: запросов: {requests_count}, администраторов: {admins_count}')
    if admins_count > 0 and requests_count > 0:
        send_report()

def send_report():
    '''
    Отправка отчета на почту администраторам
    '''
    # Формирование списка адресов
    emails = [admin.email for admin in Admin.query.all()]
    # Формирование письма
    body_html = 'Отчет во вложении'
    msg = MIMEMultipart()
    msg['Subject'] = 'Отчет о запросах гимнов стран'
    msg['From'] = conf.SMTP_USER
    msg.attach(MIMEText(body_html, 'html'))
    pdf = get_pdf()
    if pdf:
        part = MIMEApplication(pdf, _subtype = 'pdf', _encoder = encoders.encode_base64)
        part.add_header('Content-Disposition', 'attachment', filename='report.pdf')
        msg.attach(part)
        # Рассылка писем
        for email in emails:
            msg['To'] = email
            with smtplib.SMTP_SSL(conf.SMTP_HOST, conf.SMTP_PORT) as smtp:
                smtp.login(conf.SMTP_USER, conf.SMTP_PASS)
                smtp.send_message(msg)
            if not conf.TESTING:
                sleep(1) # чтобы не забанили
    else:
        sentry_sdk.capture_message(f'ms7: пустой pdf')

def get_pdf() -> bytes:
    '''
    Формирование pdf
    '''
    class HTML2PDF(FPDF, HTMLMixin):
        pass
    pdf = HTML2PDF()
    pdf.add_page()
    pdf.write_html(get_html_table())
    return pdf.output('output_file.pdf', 'S')

def get_html_table() -> str:
    '''
    Формирование html-таблицы для отчета
    '''
    html = '''
        <table border="0" align="center" width="100%">
        <thead><tr>
            <th width="10%">#</th>
            <th width="25%">datetime</th>
            <th width="40%">email</th>
            <th width="25%">country</th>
        </tr></thead>
        <tbody>'''
    for i, r in enumerate(HymnRequest.query.all()):
        dt = r.dt.strftime('%d.%m.%Y %H:%M')
        html += f'<tr><td>{i+1}</td><td>{dt}</td><td>{r.email}</td><td>{r.country}</td></tr>'
    html += '''
        </tbody>
        </table>
    '''
    return html