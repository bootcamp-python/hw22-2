import datetime
import sqlalchemy as sa
from application.app import db_model

class HymnRequest(db_model):
    '''
    Запрос гимна
    '''
    __tablename__ = 'hymn_requests'
    id      = sa.Column(sa.Integer, primary_key=True, unique=True)
    country = sa.Column(sa.String(50), nullable=False)
    email   = sa.Column(sa.String(50), nullable=False)
    dt      = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.today())

    def __init__(self, country, email) -> None:
        self.country = country
        self.email = email

    def __str__(self) -> str:
        return f'{self.id}'

class Admin(db_model):
    '''
    Администраторы
    '''
    __tablename__ = 'admins'
    id    = sa.Column(sa.Integer, primary_key=True, unique=True)
    email = sa.Column(sa.String(50), nullable=False)

    def __init__(self, email) -> None:
        self.email = email

    def __str__(self) -> str:
        return f'{self.id}'
