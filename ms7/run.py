import schedule, time
from application.app import job

schedule.every(1).minutes.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)
