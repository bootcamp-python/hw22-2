import os, redis, pika, sentry_sdk, json, requests
from time import sleep
from bs4 import BeautifulSoup
import config

flask_env = os.getenv('FLASK_ENV', None)
if flask_env == 'development':
    conf = config.DevelopmentConfig()
elif flask_env == 'testing':
    conf = config.TestingConfig()
elif flask_env == 'production':
    conf = config.ProductionConfig()
else:
    raise Exception('Environment not loaded')

sentry_sdk.init(conf.SENTRY_DSN, traces_sample_rate=1.0, server_name='ms4')
sentry_sdk.capture_message(f'ms4: init: RDS_HOST={conf.RDS_HOST}, RDS_PORT={conf.RDS_PORT}, RMQ_URL={conf.RMQ_URL}')

def run(ch, method, properties, body):
    '''
    Обработка сообщения из очереди
    '''
    try:
        data = json.loads(body.decode())
        country = data['country']
        country_l = country.lower()
        sentry_sdk.capture_message(f"ms4: поиск ссылки на страну ({data['country']}, {data['email']})")
        # На всякий случай проверим кэш
        url = rds_cache_get(country_l)
        if not url:
            # Ищем ссылку на страну
            countries = {}
            wiki_url = 'https://en.wikipedia.org/wiki/List_of_countries_by_population_(United_Nations)'
            soup = BeautifulSoup(requests.get(wiki_url).text, 'html.parser')
            url = ''
            for td in soup.find_all('table')[0].find_all('td')[0::6]:
                for country_name, country_url in [(a.text.lower(), a.get('href')) for a in td.find_all('a')]:
                    if country_l == country_name and country_url[0] != '#':
                        url = 'https://en.wikipedia.org' + country_url
                        break
                if url:
                    break
            # Добавляем в кэш
            if url:
                rds_cache_mset({country_l: url})
        rmq_channel_basic_publish('urls', json.dumps({'country': country, 'url': url, 'email': data['email']}))
        sentry_sdk.capture_message(f"ms4: поиск ссылки выполнен ({country}, {url}, {data['email']})")
    except Exception as err:
        sentry_sdk.capture_message(f"ms4: ошибка в run: {err}")

if conf.TESTING:
    rds_cache = None
    rmq_channel = None
else:
    sleep(20) # задержка чтобы другие сервисы успели запуститься
    rds_cache = redis.Redis(host=conf.RDS_HOST, port=conf.RDS_PORT)
    rmq_parameters = pika.URLParameters(conf.RMQ_URL)
    rmq_connection = pika.BlockingConnection(rmq_parameters)
    rmq_channel = rmq_connection.channel()
    rmq_channel.queue_declare(queue='urls')
    rmq_channel.queue_declare(queue='countries')
    rmq_channel.basic_consume(queue='countries', consumer_tag='ms4', on_message_callback=run, auto_ack=True)

def rds_cache_get(country: str) -> str:
    '''
    Поиск в кэше Redis
    '''
    res = rds_cache.get(country)
    if res:
        return res.decode()
    else:
        return None

def rds_cache_mset(body):
    '''
    Добавление в кэш Redis
    '''
    rds_cache.mset(body)

def rmq_channel_basic_publish(routing_key: str, body: str) -> None:
    '''
    Помещение сообщения в очередь RabbitMQ
    '''
    rmq_channel.basic_publish(exchange='', routing_key=routing_key, body=body)
