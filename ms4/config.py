import os

class Config:
    DEBUG   = False
    TESTING = False
    
    SENTRY_DSN = os.getenv('SENTRY_DSN')

    RDS_HOST = os.getenv('RDS_HOST')
    RDS_PORT = os.getenv('RDS_PORT')

    RMQ_HOST = os.getenv('RMQ_HOST')
    RMQ_PORT = os.getenv('RMQ_PORT')
    RMQ_USER = os.getenv('RMQ_USER')
    RMQ_PASS = os.getenv('RMQ_PASS')
    RMQ_URL  = f'amqp://{RMQ_USER}:{RMQ_PASS}@{RMQ_HOST}:{RMQ_PORT}'

class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True
