from application.app import rmq_channel

if __name__ == '__main__':
    rmq_channel.start_consuming()
