import pytest, mock, json
from application.app import run

def test_run():
    # Есть страна в кэше
    with mock.patch('application.app.rds_cache_get', return_value = 'http://russia') as rds_cache_get_mock:
        with mock.patch('application.app.rds_cache_mset') as rds_cache_mset_mock:
            with mock.patch('application.app.rmq_channel_basic_publish') as rmq_channel_basic_publish_mock:
                body = str.encode(json.dumps({'country': 'russia', 'email': 'user@user.ru'}))
                run(None, None, None, body)
                rds_cache_get_mock.assert_called_once_with('russia')
                body = json.dumps({'country': 'russia', 'url': 'http://russia', 'email': 'user@user.ru'})
                rmq_channel_basic_publish_mock.assert_called_once_with('urls', body)
                rds_cache_mset_mock.assert_not_called()
    # Нет страны в кэше, cтрана существует
    with mock.patch('application.app.rds_cache_get', return_value = None) as rds_cache_get_mock:
        with mock.patch('application.app.rds_cache_mset') as rds_cache_mset_mock:
            with mock.patch('application.app.rmq_channel_basic_publish') as rmq_channel_basic_publish_mock:
                body = str.encode(json.dumps({'country': 'russia', 'email': 'user@user.ru'}))
                run(None, None, None, body)
                rds_cache_get_mock.assert_called_once_with('russia')
                rmq_channel_basic_publish_mock.assert_called_once()
                rds_cache_mset_mock.assert_called_once()
    # Нет страны в кэше, cтрана не существует (т.е. не находится)
    with mock.patch('application.app.rds_cache_get', return_value = None) as rds_cache_get_mock:
        with mock.patch('application.app.rds_cache_mset') as rds_cache_mset_mock:
            with mock.patch('application.app.rmq_channel_basic_publish') as rmq_channel_basic_publish_mock:
                body = str.encode(json.dumps({'country': 'нет такой страны', 'email': 'user@user.ru'}))
                run(None, None, None, body)
                rds_cache_get_mock.assert_called_once_with('нет такой страны')
                body = json.dumps({'country': 'нет такой страны', 'url': '', 'email': 'user@user.ru'})
                rmq_channel_basic_publish_mock.assert_called_once_with('urls', body)
                rds_cache_mset_mock.assert_not_called()

if __name__ == "__main__":
    pytest.main()
